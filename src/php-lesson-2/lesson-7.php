<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ループ処理(foreach文) リファレンス渡し</h1>
    <?php
        $colors = ['赤', '青', '黒'];
        foreach ($colors as $color) {
            echo "<p>{$color}</p>";
        }

        $towns = [
            'tokyo' => '東京',
            'yamaguchi' => '山口',
            'sizuoka' => '静岡'
        ];
        foreach ($towns as $key => $value) {
            echo "<p>キー:{$key} 値:{$value}</p>";
        }
    ?>
    <?php
        $numbers = [3, 5, -1, 2];
        foreach ($numbers as &$number) {
            if ($number < 0) {
                $number = 0;
            }
        }
        // ループ処理が終わったらunset命令で消す。
        unset($number);
    ?>
    <pre><?php print_r($numbers);?></pre>
</body>
</html>