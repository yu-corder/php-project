<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>条件分岐</h1>
    <?php
        // 単一の条件式
        $point = 90;
        if ($point >= 80) {
            $message = 'ハイスコアです。';
        } else {
            $message = '通常スコアです。';
        }
    ?>
    
    <p>message: <?=$message?></p>

    <?php 
        // 論理演算子を使った複合的な条件式
        $point = -30;
        if ($point < 0 || $point > 100) {
            $message = 'ポイント値が不正です。';
        } else {
            $message = 'ポイントは正常';
        }
    ?>
    <p>message: <?=$message?></p>

    <?php
        // 真偽値を返すPHPの命令を使った条件式
        $point = '90A';
        if (is_numeric($point)) {
            $message = 'ポイントは数値です。';
        } else {
            $message = 'ポイントは数値ではありません。';
        }
    ?>
    <p>message: <?=$message?></p>

    <?php
        //数値を返すPHPの命令を使った条件式。暗黙的に真偽値に変換される。
        $places = ['東京', '京都', 'ニューヨーク'];
        if (count($places)) {
            $message = '場所が一つ以上存在します。';
        } else {
            $message = '場所が一つも存在しません。';
        }
    ?>
    <p>message: <?=$message?></p>

    <?php
        // 空文字もNULLも正しく判断できる。
        $value = null;
        if (is_null($value) || $value === '') {
            echo '変数はからです。', PHP_EOL;
        } else {
            echo '変数は空ではありません。', PHP_EOL;
        }
    ?>
    <?php
        // 空文字もNULLも正しく判断できる。
        $value = '';
        if (is_null($value) || $value === '') {
            echo '変数はからです。', PHP_EOL;
        } else {
            echo '変数は空ではありません。', PHP_EOL;
        }
    ?>
    <?php 
        // 空文字もNULLも正しく判断できる。ぜろも判断できる。
        $value = 0;
        if (is_null($value) || $value === '') {
            echo '変数はからです。', PHP_EOL;
        } else {
            echo '変数は空ではありません。', PHP_EOL;
        }
    ?>
</body>
</html>