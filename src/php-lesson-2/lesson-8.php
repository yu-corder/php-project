<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ループ処理(for文)</h1>
    <pre>
    <?php
        for ($i = 1; $i <= 3; $i++) {
            echo "Hello! ({$i}回目)", PHP_EOL;
        }
    ?>
    </pre>

    <pre>
    <?php
        $lines = [
            'いろはにほへと',
            'チリぬるを',
            '若よたれそ'
        ];
        for ($lineNumber = 1; $lineNumber <= count($lines); $lineNumber++) {
            echo $lineNumber, '行目:', $lines[$lineNumber - 1], PHP_EOL;
        }
    ?>
    </pre>

    <pre>
    <?php
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 2; $j++) {
                echo '$i;', $i, '$j;', $j, PHP_EOL;
                if ($i === 2 && $j === 1) {
                    echo 'これが最後のループ処理です。', PHP_EOL;
                }
            }
        }
    ?>
    </pre>
    
</body>
</html>