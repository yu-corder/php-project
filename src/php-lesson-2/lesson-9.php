<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ループ処理(while文) break continue</h1>
    <pre>
    <?php
        $num = 100;
        while ($num < 200) {
            echo $num, PHP_EOL;
            $num += 30;
        }
        echo '$numが200を超えたためループを抜けました。';
    ?>
    </pre>

    <pre>
    <?php
        $total = 0;
        $numbers = [10, 2, -5, 3, 'abc', 6, 1];
        echo '正の整数を対象に配列の要素を足算します...', PHP_EOL;
        foreach ($numbers as $number) {
            if (!is_numeric($number)) {
                echo "数値ではない値を検出したため計算を中止します。({$number})", PHP_EOL;
                break;
            }
            if ($number < 0) {
                echo "マイナス値は計算しません.({$number})", PHP_EOL;
                continue;
            }
            $total += $number;
        }
        echo "合計: {$total}", PHP_EOL;
    ?>
    </pre>
    <?php $colors = ['red', 'gray', 'yellow']; ?>
    <?php foreach ($colors as $color): ?>
        <?php if ($color === 'red'): ?>
            <span style = "color:red">赤</span>
        <?php elseif ($color === 'yellow'): ?>
            <span style = "color:yellow">黄</span>
        <?php else : ?>
            <sapn style = "color:black">その他</span>
        <?php endif; ?>
    <?php endforeach;?>
</body>
</html>