<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>条件分岐2 switch構文 exit die</h1>
    <?php
        $message = '';
        $extension = 'gif';
        switch ($extension) {
            case 'jpg' :
                $message = 'jpg画像です。';
                break;
            case 'png' :
                $message = 'png画像です。';
                break;
            case 'gif' :
                $message = 'gif画像です。';
                break;
            case 'bmp' :
            case 'svg' :
                $message = 'bmpまたはsvg画像です。';
                break;
            default :
                $message = 'その他の形式です。';
        }
    ?>
    <p>message: <?= $message ?></p>

    <?php
        $score = -100;
        if ($score < 0) {
            die('スコアは正の数でなければなりません。');
        }
        echo 'スコアは:', $score, '点です。';
    ?>
</body>
</html>