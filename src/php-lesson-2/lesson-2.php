<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>三項演算子 null合体演算子</h1>
    <pre>
    <?php
        /* 三項演算子を用いた例(1) */
        $greeting = null;
        $message = $greeting === null ? 'hello' : $greeting;
        echo '挨拶は', $message, PHP_EOL;

        /* 三演算子を用いた例(2) */
        $greeting = 'Good Morning';
        $message = $greeting === null ? 'hello' : $greeting;
        echo '挨拶は', $message, PHP_EOL;

        /* null合体演算子を用いた例(1) */
        $greeting = null;
        $message = $greeting ?? 'Hello';
        echo '挨拶は', $message, PHP_EOL;

        /* null合体演算子を用いたれい(2) */
        $greeting = 'Good Morning';
        $message = $greeting ?? 'Hello';
        echo '挨拶は', $message, PHP_EOL;
    ?>
</body>
</html>