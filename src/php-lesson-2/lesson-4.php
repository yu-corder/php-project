<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ビット積</h1>
    <?php
        //ボタンごとの行番号
        define('BIT_RED', 1 << 0); //赤 0001
        define('BIT_BLUE', 1 << 1); // 青 0010
        define('BIT_YELLOW', 1 << 2); // 黄色 0100
        define('BIT_GREEN', 1 << 3); // 緑 1000

        $colors = 0;
        $colors = BIT_BLUE | BIT_YELLOW; // 0110 colors

        $statuses = [
            'red' => ($colors & BIT_RED) > 0,
            'blue' => ($colors & BIT_BLUE) > 0,
            'yellow' =>($colors & BIT_YELLOW) > 0,
            'green' =>($colors & BIT_GREEN) > 0
        ];
    ?>
    <p>赤はonか？<?php var_dump($statuses['red']);?></p>
    <p>青はonか？<?php var_dump($statuses['blue']);?></p>
    <p>黄色はonか？<?php var_dump($statuses['yellow']);?></p>
    <p>緑はonか？<?php var_dump($statuses['green']);?></p>
</body>
</html>