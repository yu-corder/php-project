<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>比較演算子</h1>
    <?php $point = 85; ?>
    <?php
        var_dump($point == 85);
        var_dump($point == '85');
        var_dump($point === 85);
        var_dump($point === '85');
        var_dump($point != 85);
        var_dump($point != '85');
        var_dump($point !== 85);
        var_dump($point !== '85');
        var_dump($point > 85);
        var_dump($point >= 85);
        var_dump($point > '85');
        var_dump($point >= '85');
    ?><br>
    <?php
        $a = 1.2340;
        $b = 1.2345;

        var_dump(abs($a - $b) < 0.1);
        var_dump(abs($a - $b) < 0.01);
        var_dump(abs($a - $b) < 0.001);
        var_dump(abs($a - $b) < 0.0001);
        var_dump(abs($a - $b) < 0.00001);
    ?><br>
    <?php
        var_dump($point <=> 85);
        var_dump($point <=> 70);
        var_dump($point <=> 99);
    ?>
</body>
</html>