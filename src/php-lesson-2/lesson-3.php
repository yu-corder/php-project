<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>論理演算子</h1>
    <?php
        $temperature = 39;
        
        // 適温の範囲内であるかを調べる
        $isSuitable = $temperature >= 40 && $temperature <= 41;
        var_dump($isSuitable);

        // 適温の範囲外であるかを調べる1
        $isNotSuitable = $temperature < 40 || $temperature > 41;
        var_dump($isNotSuitable);

        // 適温の範囲外であるかを調べる2
        $isNotSuitable = !($temperature >= 40 && $temperature <= 41);
        var_dump($isNotSuitable);
    ?><br>

    <?php
        //define('URL_BASE', 'http://www.example.com');
        defined('URL_BASE') || define('URL_BASE', 'http://default.example.com');
        echo 'URL_BASE is: ', URL_BASE;
    ?>

</body>
</html>