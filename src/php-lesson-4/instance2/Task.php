<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php
        class Task
        {
            public $name;
            public $priority;
            public $progress;

            public function isCompleted(): bool
            {
                return $this->progress >= 100;
            }
        }
    ?>
</body>
</html>