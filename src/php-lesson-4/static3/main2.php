<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 静的プロパティ/静的メソッド</h1>
    <?php
        //何らかのクラス
        class SomeClass
        {
            //インスタンスプロパティ
            private $instanceProperty;

            public function instanceMethod()
            {
                echo 'instanceMethod() called.<br>';
            }

            //静的メソッド
            public static function staticMethod()
            {
                //自分自身のインスタンスを作り インスタンスメソッドを呼び出す
                $someClass = new SomeClass();
                $someClass->instanceMethod();

                //外部クラスのインスタンスを作り、インスタンスメソッドを呼び出す
                $externalClass = new ExternalClass();
                $externalClass->externalMethod();
            }
        }
        class ExternalClass
        {
            public function externalMethod()
            {
                echo 'externalMethod() called. <br>';
            }
        }
        SomeClass::staticMethod();
    ?>
</body>
</html>