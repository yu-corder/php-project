<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 静的プロパティ/静的メソッド</h1>
    <?php
        //何らかのクラス
        class SomeClass
        {
            //インスタンスプロパティ
            private $instanceProperty;

            //静的メソッド
            public static function staticMethod()
            {
                //インスタンスプロパティにアクセスする
                //エラーUsing $this when not in object context
                $this->instanceProperty;
            }
        }

        //メインルーチン
        SomeClass::staticMethod();
    ?>
</body>
</html>