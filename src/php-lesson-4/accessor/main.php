<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 アクセサ</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';
        $task = new Task();

        $task->setName('パスポートの更新');
        echo 'タスク名:', $task->getName(), '<br>';

        //タスクの進行度を１２０%にする
        $task->setProgress(120);
        echo '進行度:', $task->getProgress(), '<br>';

        //タスクの進行度を80%にした後、進行度を０に戻す
        $task->setProgress(80);
        $task->clearProgress();
        echo '進行度:', $task->getProgress(), '<br>';

        //タスクを完了させる。
        $task->completeProgress();
        echo '進行度:', $task->getProgress(), '<br>';
    ?>
</body>
</html>