<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 継承</h1>
    <?php
        require_once dirname(__FILE__) . '/DigitalClock.php';
        $currentTime = strtotime('2021-06-01 21:14');
        $digitalClock = new DigitalClock();
        $digitalClock->setTime($currentTime);
        echo $digitalClock->show();
    ?>
    
</body>
</html>