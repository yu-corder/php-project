<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 継承</h1>
    <?php
        require_once dirname(__FILE__) . '/Canvas.php';
        require_once dirname(__FILE__) . '/Digital.Clock.php';
        require_once dirname(__FILE__) . '/AnalogClock.php';
        $canvas = new Canvas();

        $currentTime = strtotime('2021-06-01 21:29');
        $digitalClock = new DigitalClock();
        $digitalClock->setTime($currentTime);
        $canvas->drawClock($digitalClock);

        $analogClock = new AnalogClock();
        $analogClock->setTime($currentTime);
        $canvas->drawClock($analogClock);
    ?>
</body>
</html>