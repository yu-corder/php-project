<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 オーバーライド</h1>
    <?php
        class Clock
        {
            final public function show()
            {
                echo 'Show Clock...';
            }

        }

        class DigitalClock extends Clock //show メソッドはオーバーライドふか
        {
            //public function show()
            //{
               // echo 'Show Digital Clock';
            //}
        }
    ?>
</body>
</html>