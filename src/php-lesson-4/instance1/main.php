<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';


        //TODOタスク パポートの更新を作る
        $task1 = new Task();
        $task1->name = 'パスポートの更新';
        $task1->progress = 100;

        //TODOタスク 食材の買い出し
        $task2 = new Task();
        $task2->name = '食材の買い出し';
        $task2->progress = 50;

        
        if ($task1->isCompleted() === true) {
            echo $task1->name, 'は完了しました。<br>';
        } else {
            echo $task1->name, 'は未完了です。<br>';
        }

        
        if ($task2->isCompleted() === true) {
            echo $task2->name, 'は完了しました。<br>';
        } else {
            echo $task2->name, 'は未完了です。<br>';
        }
    ?>
</body>
</html>