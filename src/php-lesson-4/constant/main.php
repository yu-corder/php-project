<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 オブジェクト定数</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';
        $task = new Task();
        $task->priority = Task::PRIORITY_HIGE;
        echo '優先度:', $task->getPriorityAsString(), '</br>';
    ?>
    
</body>
</html>