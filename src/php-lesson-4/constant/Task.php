<?php
declare(strict_types=1);

class Task
{
    //優先度を表す定数
    public const PRIORITY_LOW = 0;
    public const PRIORITY_MIDDLE = 1;
    public const PRIORITY_HIGE = 2;

    //タスク名
    public $name;

    //優先度
    public $priority;

    //進行度
    public $progress;

    public function getPriorityAsString(): string
    {
        switch ($this->priority) {
            case self::PRIORITY_LOW :
                return '低';
                break;
            case self::PRIORITY_MIDDLE :
                return '中';
                break;
            case self::PRIORITY_HIGE :
                return '高';
                break;
        }
    }
}