<?php
declare(strict_types=1);

//歩けるものインターフェース
interface Walk
{
    //歩くアニメーションをするメソッド
    public function walk(): void;
}