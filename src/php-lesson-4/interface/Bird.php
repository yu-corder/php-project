<?php
declare(strict_types=1);
require_once dirname(__FILE__) . '/Flyable.php';
require_once dirname(__FILE__) . '/Walk.php';

//鳥クラス
class Bird implements Flyable, Walk
{
    public function fly(): void
    {
        //鳥固有の飛ぶアニメーションの処理を記述する
        echo 'Bird is Flying...';
    }

    //歩くメソッド
    public function walk(): void
    {
        //鳥固有の歩くアニメーションの処理を記述する
        echo 'Bird is walking...';
    }
}