<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 interface</h1>
    <?php
        require_once dirname(__FILE__) . '/Bird.php';
        $bird = new Bird();
        $bird->fly();
        $bird->walk();
    ?>
</body>
</html>