<?php
declare(strict_types=1);

require_once dirname(__FILE__) . '/Flyable.php';

class Airplane implements Flyable
{
    //飛ぶメソッド
    public function fly(): void
    {
        echo 'Airplane is flying...';
    }
}