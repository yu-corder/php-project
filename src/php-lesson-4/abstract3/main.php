<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 オーバーライド</h1>
    <?php
        require_once dirname(__FILE__) . '/DigitalClock.php';
        $currentTime = strtotime('2021-06-01 21:01');
        $digitalClock = new DigitalClock();
        $digitalClock->setTime($currentTime);//オーバーライドされたsetTime()呼び出し
        echo $digitalClock->getColor();
    ?>
</body>
</html>