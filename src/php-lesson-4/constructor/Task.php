<?php
declare(strict_types=1);

class Task
{
    //タスク名
    public $name;

    //優先度
    public $prioity;

    //進行度
    public $progress;

    //コンストラクタ
    public function __construct(string $name/*, int $prioity ,int $progress*/)
    {
        $this->name = $name;
        $this->priority = 1/*$prioity*/;
        $this->progress = 0/*$progress*/;
    }
}