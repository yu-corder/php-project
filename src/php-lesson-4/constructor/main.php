<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 コンストラクタ</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';
        $task = new Task('パスポートの更新');
        echo $task->name, '<br>';
        echo $task->priority, '<br>';
        echo $task->progress, '<br>';
    ?>
</body>
</html>