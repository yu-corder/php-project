<?php
declare(strict_types=1);

//エクセルの列名を変換するクラス
class ExcelColumnconverter
{
    //数値で表された列の番号を元に、アルファベット表記の列名を返す
    public static function calcAlphabetColumnName(int $number): string
    {
        return chr(ord('A') + $number);
    }

    //アルファベット表記の列名を元に、数値で表された列の番号を渡す
    public static function calcNumberColumnName(string $alphabet): int
    {
        return ord($alphabet) - ord('A');
    }
}