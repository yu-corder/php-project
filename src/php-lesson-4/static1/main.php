<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>オブジェクト指向 静的メソッド</h1>
    <?php
        require_once dirname(__FILE__) . '/ExcelColumnConverter.php';
        echo ExcelColumnConverter::calcAlphabetColumnName(3), '<br>';
        echo ExcelColumnConverter::calcNumberColumnName('B'), '<br>';
    ?>
</body>
</html>