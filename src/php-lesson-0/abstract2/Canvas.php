<?php
declare(strict_types=1);
require_once dirname(__FILE__) . '/Clock.php';

class Canvas
{
    public static $backgroundColor;


    public function drawClock(DigitalClock $digitalClock): void
    {
        echo $digitalClock->show(), '<br>';
        echo $digitalClock->getColor();
    }

}