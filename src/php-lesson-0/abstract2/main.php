<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Canvas.php';
        require_once dirname(__FILE__) . '/DigitalClock.php';
        require_once dirname(__FILE__) . '/AnalogClock.php';

        $canvas = new Canvas();
        Canvas::$backgroundColor = 'blue';
        $currentTime = strtotime(date('H:i'));
        $digitalClock = new DigitalClock($currentTime);
        $canvas->drawClock($digitalClock);
    ?>
</body>
</html>