<?php
declare(strict_types=1);
require_once dirname(__FILE__) . '/Clock.php';

class AnalogClock extends Clock
{
    public function show(): string
    {
        $minutedHand = 90;
        $hourHand = 157.5;
        return "長針: {$minutedHand} 短針: {$hourHand}";
    }
}