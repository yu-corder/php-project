<?php
declare(strict_types=1);

abstract class Clock
{
    protected $time;

    public function __construct(int $time)
    {
        $this->time = $time;
    }

    abstract public function show(): string;

    public function setTime(int $time): void
    {
        $this->time = $time;
    }

    public function getTime(): string
    {
        return $this->time;
    }
}