<?php
declare(strict_types=1);
require_once dirname(__FILE__) . '/Clock.php';
require_once dirname(__FILE__) . '/Canvas.php';

class DigitalClock extends Clock
{
    public function show(): string
    {
        return date('H:i', $this->time);
    }

    public function getColor(): string
    {
        return Canvas::$backgroundColor;
    }
}