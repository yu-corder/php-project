<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/ShoppingPoint.php';
        //曜日ポイントクラス
        class WeekDayPoint
        {
            //今日が金曜日なら、１ポイント加算するメソッド
            public function addWeekDayPoint(string $youbi)
            {
                if ($youbi === 'Fri') {
                    ShoppingPoint::countUpPoint();
                }
            }
        }

        //1000以上の購入金額の時に、１ポイント加算する関数
        function addPricePoint(int $price) 
        {
            if ($price >= 1000) {
                ShoppingPoint::countUpPoint();
            }
        }

        //初期ポイントはゼロとする
        ShoppingPoint::$point = 0;

        //購入しただけで無条件に１ポイント加算する
        ShoppingPoint::countUpPoint();

        //曜日によって１ポイント加算する
        $weekDayPoint = new WeekDayPoint();
        $weekDayPoint->addWeekDayPoint('Fri');

        //購入時金額によって１ポイント加算する
        addPricePoint(5000);
        echo '購入ポイント:', ShoppingPoint::$point;
    ?>
</body>
</html>