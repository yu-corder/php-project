<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        class StaticMembers
        {
            public static $stNum = 0;
            public $num = 0;

            public function countUp(int $plus):void
            {
                self::$stNum += $plus;
                $this->num += $plus;
            }
        }

        $staticA = new StaticMembers();
        $staticB = new StaticMembers();

        $staticA->countUp(10);
        echo $staticA::$stNum . '<br>';
        echo $staticA->num . '<br>';

        $staticB->countUp(80);
        echo $staticB::$stNum . '<br>';
        echo $staticB->num . '<br>';

        //静的プロパティのみ表示
        echo StaticMembers::$stNum;
    ?>
</body>
</html>