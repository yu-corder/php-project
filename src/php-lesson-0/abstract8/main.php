<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        abstract class Clock
        {
            protected $time;

            public function __construct(int $time)
            {
                $this->time = $time;
            }

            final public function show()
            {
                return 'Show Clock...';
            }
        }

        class DigitalClock extends Clock
        {
            private $color;

            public function __construct(int $time, string $color)
            {
                $this->color = $color;
                parent:: __construct;
            }

            //finalを親クラスのshowメソッドにつけているためオーバーライドふか
            //public function show()
            //{
            //    return 'Show DigitalClock...';
            //}
        }
    ?>
</body>
</html>