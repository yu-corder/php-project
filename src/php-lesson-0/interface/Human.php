<?php
declare(strict_types=1);

require_once dirname(__FILE__) . '/Walkable.php';

class Human implements Walkble
{
    public function walk(): void
    {
        echo 'Human is walking...';
    }
}