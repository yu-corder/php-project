<?php
declare(strict_types=1);
interface Walkble
{
    public function walk(): void;
}