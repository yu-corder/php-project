<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>インターフェース</h1>
    <?php
        require_once dirname(__FILE__) . '/Sky.php';
        require_once dirname(__FILE__) . '/Bird.php';
        require_once dirname(__FILE__) . '/Airplane.php';
        require_once dirname(__FILE__) . '/Land.php';
        require_once dirname(__FILE__) . '/Human.php';

        //空を用意する
        $sky = new Sky();

        //陸を用意する
        $land = new Land();

        //人を用意する
        $human = new Human();
        $land->draw($human);

        //鳥を用意する
        $bird = new Bird();
        $sky->draw($bird);
        $land->draw($bird);

        //飛行機を用意する
        $airplane = new Airplane();
        $sky->draw($airplane);


    ?>
</body>
</html>