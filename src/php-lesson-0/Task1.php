<?php
declare(strict_types=1);

//タスククラス
class Task
{
    //タスク名
    public $name;

    //優先度(0は低い、１は中、２は高い)
    public $priority;

    //進行度
    public $progress;
}