<?php
declare(strict_types=1);
class Task
{
    private $name;
    private $priority;
    private $progress;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->priority = 1;
        $this->progress = 0;
    }

    public function getName(): string
    {
        return $this->name;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getProgress(): int
    {
        return $this->progress;
    }
    public function setProgress(int $progress): void
    {
        $this->progress = $progress;
    }
}