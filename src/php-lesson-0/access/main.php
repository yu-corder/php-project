<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        class SomeClass
        {
            public $propertyA;
            private $propertyB;

            public function methodA(): void
            {
                echo 'methodA called.<br>';
                $this->methodB();//自クラス内からであればアクセス可能
            }

            private function methodB(): void
            {
                echo 'methodB called.<br>';
            }
        }
        $someInstance = new SomeClass();
        $someInstance->propertyA = 'A';
        $someInstance->methodA();
        $someInstance->propertyB = 'B';
        $someInstance->methodB();
    ?>
</body>
</html>