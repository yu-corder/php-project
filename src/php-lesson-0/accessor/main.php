<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';

        $task1 = new Task('アイウエオ');
        $task1->setName('パスポートの更新');
        echo $task1->getName(), '<br>';

        //タスクの進行度を１２０にする
        $task1->setProgress(120);
        echo $task1->getProgress(), '<br>';

        //タスクの進行度を８０％にした後、進行度を０に戻す
        $task1->setProgress(80);
        $task1->clearProgress();
        echo $task1->getProgress(), '<br>';

        //タスクを完了させる
        $task1->completeProgress();
        echo $task1->getProgress(), '<br>';
    ?>
</body>