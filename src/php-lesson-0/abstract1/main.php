<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/DigitalClock.php';
        require_once dirname(__FILE__) . '/AnalogClock.php';
        $currentTime = strtotime('2021-06-01 17:20');
        $digitalClock = new DigitalClock($currentTime);
        echo $digitalClock->show(), '<br>';
        $digitalClock->setTime(strtotime(date("H:i")));
        echo $digitalClock->show();
    ?>
</body>
</html>