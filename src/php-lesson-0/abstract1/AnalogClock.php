<?php
declare(strict_types=1);
require_once dirname(__FILE__) . '/Clock.php';
class AnalogClock extends Clock
{
    //クロッククラスの抽象メソッドshow()はサブクラスで必ず実装
    public function show(): string
    {
        $minutedHand = 90;
        $hourHand = 157.5;
        return "長針: {$minutedHand} 短針: {$hourHand}";
    }
}