<?php
declare(strict_types=1);

class Task
{
    public const PR_LOW = 0;
    public const PR_MIDDLE = 1;
    public const PR_HIGH = 2;

    private $name;
    private $priority;
    private $progress;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->priority = 1;
        $this->progress = 0;
    }

    public function getName(): string
    {
        return $this->name;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    public function getProgress(): int
    {
        return $this->progress;
    }
    public function setProgres(int $progress): void
    {
        $this->progress = $progress;
    }

    //優先度を低いから高いの文字列で取得する
    public function getPriorityAsString(): string
    {
        switch ($this->priority) {
            case self::PR_LOW :
                return '低い';
                break;
            case self::PR_MIDDLE :
                return '中';
                break;
            case self::PR_HIGH :
                return '高い';
                break;
            default :
                return '定数が無効です。';
        }
    }

}