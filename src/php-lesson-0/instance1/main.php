<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';

        $task1 = new Task();
        $task1->setName('パスポートの更新');
        $task1->setProgress(100);
        $task1->setPriority(2);
        echo $task1->getName(), '<br>';
        if ($task1->getPriority() >= 2) {
            echo '優先度: 高い<br>';
        } elseif ($task->getPriority() >= 1) {
            echo '優先度: 中<br>';
        } else {
            echo '優先度: 低い<br>';
        }
        if ($task1->isCompleted() === true) {
            echo $task1->getName(), 'は完了しました。<br>';
        } else {
            echo $task1->getName(), 'は未完了です<br>';
        }
        $task2 = new Task();
        $task2->setName('食材の買い出し');
        $task2->setPriority(8);
        $task2->setProgress(50);
        echo $task2->getName(), '<br>';
        switch ($task2->getPriority()) {
            case 2 :
                echo '優先度: 高い<br>';
                break;
            case 1 :
                echo '優先度: 中<br>';
                break;
            case 0 :
                echo '優先度: 低い<br>';
                break;
            default :
                echo '入力された値が無効です。<br>';
        }

        if ($task2->isCompleted() === true) {
            echo $task2->getName(), 'は完了しました。<br>';
        } else {
            echo $task2->getName(), 'は未完了です。<br>';
        }
    ?>
</body>
</html>