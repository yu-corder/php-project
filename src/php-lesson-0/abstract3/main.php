<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/DigitalClock.php';
        $currentTime = strtotime('2018-08-22 17:15');
        $digitalClock = new DigitalClock($currentTime);
        $digitalClock->setTime(strtotime(date('H:i')));
        echo $digitalClock->getColor();
    ?>
</body>
</html>