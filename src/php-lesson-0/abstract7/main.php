<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        abstract class Clock
        {
            private $time;

            public function __construct(int $time)
            {
                echo 'Clockクラスのコンストラクタが呼ばれました。<br>';
                $this->time = $time;
            }
        }
        class DigitalClock extends Clock
        {
            private $color;
            public function __construct(int $time, string $color)
            {
                echo 'DigitalClockクラスのコンストラクタが呼ばれました。<br>';
                $this->color = $color;
                parent::__construct($time);
            }
        }

        $currentTime = strtotime(date('H:i'));
        $digitalClock = new DigitalClock($currentTime, 'blue');

    ?>
</body>
</html>