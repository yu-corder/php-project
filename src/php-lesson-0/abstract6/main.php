<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        abstract class Clock
        {
            private $time;

            public function __construct(int $time)
            {
                $this->time = $time;
            }
            
            public function setTime(int $time): void
            {
                echo 'ClockクラスのsetTimeメソッドが呼ばれました。<br>';
            }
        }

        class DigitalClock extends Clock
        {
            public function setTime(int $time): void
            {
                echo 'DigitalClockクラスのsetTimeメソッドが呼ばれました。<br>';
                parent::setTime($time);
            }
        }

        $currentTime = strtotime(date('H:i'));
        $digitalClock = new DigitalClock($currentTime);
        $digitalClock->setTime($currentTime);
    ?>
</body>
</html>