<?php
declare(strict_types=1);
require_once dirname(__FILE__) . '/Task.php';

class TaskSheet
{
    private $tasks = [];

    public function addTask(Task $task): string
    {
        $this->tasks[] = $task;
        return $task->getName();
    }

    public function getTaskSheet(): array
    {
        return $this->tasks;
    }
}