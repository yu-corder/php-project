<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Task.php';
        require_once dirname(__FILE__) . '/TaskSheet.php';

        $taskSheet = new TaskSheet();

        $task1 = new Task();
        $task1->setName('パスポートの更新');
        $task1->setProgress(100);

        $task2 = new Task();
        $task2->setName('食材の買い出し');
        $task2->setProgress(50);

        echo $taskSheet->addTask($task1), 'を追加しました。<br>';
        echo $taskSheet->addTask($task2), 'を追加しました。<br>';
        foreach ($taskSheet->getTaskSheet() as $task) {
            if ($task->isCompleted()) {
                echo '<b>', $task->getName(), '</b><br>';
            } else {
                echo $task->getName(), '<br>';
            }
        }
    ?>
</body>
</html>