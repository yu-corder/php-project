<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php
        //タスククラス
        class Task
        {
            //タスク名
            public $name;

            //付加的な情報
            private $extras = [];

            //プロパティをセットするマジックメソット
            public function __set($name, $value)
            {
                $this->extras[$name] = $value;
            }

            //プロパティを取得するメソッド
            public function __get($name)
            {
                return $this->extras[$name];
            }

            //出力するマジックメソッド
            public function __toString()
            {
                $properties = 'タスク名:' . $this->name . '<br>';
                foreach ($this->extras as $key => $value) {
                    $properties .= $key . ':' . $value . '<br>';
                }
                return $properties;
            }
        }

        //メインルーチン
        $task = new Task();
        $task->name = '飛行機のチケット購入';
        $task->placeFrom = '羽田';
        $task->placeTo = '福岡';
        echo $task;
    ?>
</body>
</html>