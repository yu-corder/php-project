<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Office/Word/Writer.php';
        require_once dirname(__FILE__) . '/Office/Excel/Writer.php';

        $writer = new \Office\Excel\Writer();
        $writer->write();

        $writer = new \Office\Word\Writer();
        $writer->write();
    ?>
</body>
</html>