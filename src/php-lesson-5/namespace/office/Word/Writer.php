<?php
declare(strict_types=1);

namespace Office\Word;

//Wordファイルを書き出す
class Writer
{
    public function write(): void
    {
        echo 'Wordファイルを書き出します。<br>';
    }
}