<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/office/Word/Writer.php';
        use Office\Word\Writer;
        $writer = new Writer(); //WordのWriterクラスを使う
        $writer->write();
    ?>
</body>
</html>