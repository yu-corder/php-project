<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/Office/Word/Writer.php';
        require_once dirname(__FILE__) . '/Office/Excel/Writer.php';
        use Office\Word\Writer as WordWriter;
        use Office\Excel\Writer as ExcelWriter;

        $writer = new WordWriter();//WordのWriteクラスを使う
        $writer->write();

        $writer = new ExcelWriter();//ExcelのWriterクラスを使う
        $writer->write();
    ?>
</body>
</html>