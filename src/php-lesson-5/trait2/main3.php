<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/SomeTraitA.php';
        require_once dirname(__FILE__) . '/SomeTraitB.php';

        //何らかのクラス
        class SomeClass
        {
            use SomeTraitA, SomeTraitB {
                SomeTraitB::traitMethod insteadof SomeTraitA;
                SomeTraitA::traitMethod as setA;//SomeTraitAのtraitMethodを使う時はsetAという別名を使う
            }

            public function setTraitProperty() 
            {
                $this->traitMethod(null, null);
                echo $this->traitProperty, '<br>';
                $this->setA(null, null);
                echo $this->traitProperty;
            }
        }

        $someClass = new SomeClass();
        $someClass->setTraitProperty();
    ?>
</body>
</html>