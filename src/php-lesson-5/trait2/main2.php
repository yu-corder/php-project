<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/SomeTraitA.php';
        require_once dirname(__FILE__) . '/SomeTraitB.php';

        //何らかのクラス
        class SomeClass
        {
            use SomeTraitA, SomeTraitB{
                SomeTraitB::traitMethod insteadof SomeTraitA;//traitMethodメソッドを使う時はSomeTraitBを選ぶ
            }

            public function setTraitProperty()
            {
                $this->traitMethod(null, null);//SomeTraitBのメソッドを使う
                echo $this->traitProperty;
            }
        }

        $someClass = new SomeClass();
        $someClass->setTraitProperty();
    ?>
</body>
</html>