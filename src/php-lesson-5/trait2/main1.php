<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス</h1>
    <?php
        require_once dirname(__FILE__) . '/SomeTraitA.php';
        require_once dirname(__FILE__) . '/SomeTraitB.php';

        //何らかのクラス
        class SomeClass
        {
            use SomeTraitA, SomeTraitB;

            public function setTraitProperty()
            {
                //どちらのトレイトが持つtraitMethodのことかわからないためエラー
                $this->traitMethod(null, null);
                echo $this->traiteProperty;
            }
        }

        $someClass = new SomeClass();
        $someClass->setTraitProperty();
    ?>
</body>
</html>