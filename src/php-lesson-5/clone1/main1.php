<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php
        class Task
        {
            public $name;
        }

        function changeTask(Task $task)
        {
            $task->name = '散歩';
        }

        $task1 = new Task();
        $task1->name = 'パスポートの更新中';

        $task2 = clone $task1;
        $task2->name = '買い物';

        //パスポートの更新が出力される
        echo $task1->name, '<br>';

        changeTask(clone $task2);

        //パスポートの更新が出力される
        echo $task1->name, '<br>';

        //買い物が出力される
        echo $task2->name, '<br>';
    ?>
</body>
</html>