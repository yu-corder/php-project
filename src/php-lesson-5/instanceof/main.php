<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php
        require_once dirname(__FILE__) . '/TaskSheet.php';
        require_once dirname(__FILE__) . '/Task.php';
        require_once dirname(__FILE__) . '/Schedule.php';

        $taskSheet = new TaskSheet();

        $task = new Task();
        $task->name = 'パスポートの更新';
        $taskSheet->addTask($task);

        $schedule = new schedule();
        $schedule->name = '混種の金曜日';
        $taskSheet->addTask($schedule);
    ?>
</body>
</html>