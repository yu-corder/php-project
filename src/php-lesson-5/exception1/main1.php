<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>例外処理</h1>
    <?php
        require_once dirname(__FILE__) . '/TaxCalculator.php';
        $calculator = new TaxCalculator();
        $priceWithTax = $calculator->calculate(100, -0.08);
        echo '計算結果は', $priceWithTax, '円です。';
    ?>
</body>
</html>