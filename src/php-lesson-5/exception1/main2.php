<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>例外処理</h1>
    <?php
        require_once dirname(__FILE__) . '/TaxCalculator.php';

        $calculator = new TaxCalculator();
        try {
            $priceWithTax = $calculator->calculate(100, -0.08);
            echo 'Price:', $priceWithTax, '円';
        } catch (Exception $exception) {
            echo '税率金額が計算できませんでした。理由:', $exception->getMessage(), '<br>';
        } finally {
            $calculator->reset();
        }
        echo 'プログラム処理を終了します。';
    ?>
</body>
</html>