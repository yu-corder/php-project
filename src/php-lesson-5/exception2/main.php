<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>例外処理</h1>
    <?php
        require_once dirname(__FILE__) . '/ClassA.php';
        try {
            $classA = new ClassA();
            $classA->methodA();
        } catch (Exception $exception) {
            echo 'メインルーチンで例外をキャッチ。エラー内容:', $exception->getMessage(), '<br>';
            echo '例外のトレース情報は以下の通りです。:<br>';
            print_r($exception->getTrace());
        }
        echo 'メインルーチンを終了します。';
    ?>
</body>
</html>