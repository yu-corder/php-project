<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>クラス トレイト</h1>
    <?php
        require_once dirname(__FILE__) . '/MemberModel.php';
        $memberModel = new MemberModel();
        $memberModel->create('001');
        $memberModel->delete('001');
        echo nl2br(file_get_contents('MemberModel.log'));
    ?>
</body>
</html>