<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php
        require_once dirname(__FILE__) . '/PageNotFoudException.php';

        //ユーザー情報を管理するデータベーステーブルにアクセスするクラス
        class UserModel
        {
            public function findProfile(int $userId): array
            {
                throw new PageNotFoundException('User profile does not exist.');
            }
        }

        //メインルーチン
        $model = new UserModel();
        try {
            $profile = $model->findProfile(1001);
        } catch (PageNotFoundException $exception) {
            echo file_get_contents('error-page-not-found.html');
            return;
        } catch (Exception $exception) {
            echo file_get_contents('error-unknown.html');
            return;
        }
    ?>
</body>
</html>