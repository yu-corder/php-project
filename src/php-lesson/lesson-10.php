<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php
        $sum = 3;
        $sum *= 5;

        $greeting = 'Hello ';
        $greeting .= 'World.';

        $greeting1 = 'Hello';
        $greeting2 = &$greeting1;
        $greeting1 = 'World';
    ?>
    <h1>複合演算子 リファレンス渡し</h1>
    <p>sum:<?=$sum?></p>
    <p>greeting:<?=$greeting?></p>
    <p>greeting1:<?=$greeting1?></p>
    <p>greeting2:<?=$greeting2?></p>
</body>
</html>