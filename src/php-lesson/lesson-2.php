<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>データ型</h1>
    <?php
        // 整数型
        $integerValue = 10;

        // 小数
        $floatValue = 12.34;

        // 文字列型
        $stringValue = "文字列";

        // 真偽値型
        $boolValue = true;

        // null
        $nullValue = null;

        //空文字(文字列型)
        $emptyStringValue = '';
    ?>
    <p>整数: <?php echo $integerValue; ?></p>
    <p>浮動小数点: <?php echo $floatValue; ?></p>
    <p>文字列型: <?php echo $stringValue; ?></p>
    <p>真偽値型: <?php var_dump($boolValue); ?></p>
    <p>NULL: <?php var_dump($nullValue); ?></p>
    <p>空文字: <?php var_dump($emptyStringValue); ?></p>
</body>
</html>