<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>配列/連想配列</h1>

    <?php
        $airports = ['Haneda', 'Narita', 'chubu'];
        $airports[] = 'Kansai';
        $airports[] = 'Naha';
        $airports[2] = 'Sendai';

        $user = [
            'name' => '戸田 優也',
            'age' => 22,
            'place' => '山口'
        ];
        $user['job'] = '伝統工芸士';
        $user['hobby'] = 'ドライブ';

        $participants = [];
        $participants[0] = '小林';
        $participants[1] = '戸田';
        $participants[2] = '橋尾';
        $participants['event'] = 'ワイン会';
        $participants['event-date'] = '02.22';
        $participants[3] = 'daiki';

        $cars = [
            [
                'name' => 'アクア',
                'company' => 'トヨタ'
            ],
            [
                'name' => 'fit',
                'company' => '本田'
            ],
            [
                'name' => 'デリカ',
                'company' => '三菱'
            ]
        ];
        $cars[] =
        [
            'name' => 'N-box',
            'company' => '本田'
        ]
    ?>
    <p><?=$airports[0]?></p>
    <p><?=$airports[1]?></p>
    <p><?=$airports[2]?></p>
    <p><?=$airports[3]?></p>
    <p><?= $airports[4] ?></p>
    <p><pre><?php print_r($airports);?></pre></p>
    <p><?=$user['name']?></p>
    <p><?=$user['age']?></p>
    <p><?=$user['place']?></p>
    <p><?=$user['job']?></p>
    <p><?=$user['hobby']?></p>
    <p><pre><?php print_r($user);?></pre></p>
    <p><pre><?php print_r($participants);?></pre></p>
    <p><?=$cars[0]['name']?> - <?=$cars[0]['company']?></p>
    <p><?=$cars[1]['name']?> - <?=$cars[1]['company']?></p>
    <p><?=$cars[2]['name']?> - <?=$cars[2]['company']?></p>
    <p><?=$cars[3]['name']?> - <?=$cars[3]['company']?></p>
    <p><pre><?php print_r($cars);?></pre></p>
    <p><?php echo count($cars);?></p>
    <p><?php echo count($cars[0]);?></p>
</body>
</html>