<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>型変換:キャスト</h1>
    <?php
        var_dump(1234);
        var_dump((string)1234);
        var_dump((int)1234.5);
        var_dump((string)true);
        var_dump((string)false);
        var_dump((bool)'true');
        var_dump((bool)'false');
        var_dump((bool)0);
        var_dump((bool)1);
        var_dump((bool)-1);
        var_dump(strval(1234));
        var_dump(intval(1234.5));
        var_dump(strval(true));
        var_dump(strval(false));
        var_dump(boolval('true'));
        var_dump(boolval('false'));
        var_dump(boolval(0));
        var_dump(boolval(1));
        var_dump(boolval(-1));
    ?>
    
</body>
</html>