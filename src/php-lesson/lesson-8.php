<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <?php

        /* 配列を合成します。*/
        $arr1 = ['a', 'b', 'c'];
        $arr2 = ['d', 'e', 'f'];
        $mergedA = $arr1 + $arr2;
        $mergedB = $arr2 + $arr1;
        $mergedC = array_merge($arr1, $arr2);
        $mergedD = array_merge($arr2, $arr1);

        
        
    ?>
    <h1>配列を合成</h1>
    * merged A: <?php print_r($mergedA)?><br>
    * merged B: <?php print_r($mergedB)?><br>
    * merged C: <?php print_r($mergedC)?><br>
    * merged D: <?php print_r($mergedD)?>


    <?php
        $arr3 = [
            'a' => 'value A',
            'b' => 'value B',
        ];
        $arr4 = [
            'a' => 'value C',
            'b' => 'value D',
            'c' => 'value E',
        ];
        $mergedE = $arr3 + $arr4;
        $mergedF = $arr4 + $arr3;
        $mergedG = array_merge($arr3, $arr4);
        $mergedH = array_merge($arr4, $arr3);
    ?>
    <h1>連想配列を合成</h1>
    * merged E: <?php print_r($mergedE)?><br>
    * merged F: <?php print_r($mergedF)?><br>
    * merged G: <?php print_r($mergedG)?><br>
    * merged H: <?php print_r($mergedH)?>
</body>
</html>