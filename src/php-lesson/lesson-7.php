<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>演算子</h1>
    <?php
        $num1 = 7 + 3;
        $num2 = 7 - 3;
        $num3 = 7 * 3;
        $num4 = 7 / 3;
        $num5 = 7 % 3;
        $num6 = 7 ** 3;
    ?>
    <p>加算: <?= $num1?></p>
    <p>減算: <?= $num2?></p>
    <p>乗算: <?= $num3?></p>
    <p>徐算: <?= $num4?></p>
    <p>あまり: <?= $num5?></p>
    <p>累乗: <?= $num6?></p>

    <?php $num = 7; ?>
    <?php $added = ++$num; ?>
    <p>前置きインクリメント時のnum: <?=$num?></p>
    <p>前置きインクリメント時のadded: <?= $added?></p>

    <?php $num = 7; ?>
    <?php $added = $num++; ?>
    <p>後置きインクリメント時のnum: <?=$num?></p>
    <p>後置きインクリメント時のadded: <?= $added?></p>
</body>
</html>