<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>文字列データの扱い</h1>
    <?php
        $value = 'world';

        // 文字列演算子(.)による値と変数の連結。
        $combined1 = 'hello ' . $value . '!';

        // 変数の展開 シングルクォート不可
        $combined2 = "hello {$value} !";

        // 代入演算子(=)と文字列演算子(.)
        $combined3 = "hello ";
        $combined3 .= $value;
        $combined3 .= '!';

        // シングルクォートないでは変数展開されない
        $combined4 = 'hello {$value} !';
    ?>
    <p><?=$combined1?></p>
    <p><?=$combined2?></p>
    <p><?=$combined3?></p>
    <p><?=$combined4?></p>

    <h1>文字列データの扱い２</h1>
    <?php
        $quoted1 = 'The Book of "PHP\'s Honkaku"';
        $quoted2 = "The Book of \"PHP's Honkaku\"";
    ?>
    <p><?=$quoted1?></p>
    <p><?=$quoted2?></p>

    <h1>文字列データの扱い３</h1>
    <?php
        $mailBody = "お問い合わせを受け付けました。\n\n";
        $mailBody .= "お問い合わせ内容:\n";
        $mailBody .= "\t商品番号 abc123について、サイズを教えてください。";
        $mailBody2 = "お問い合わせを受け付けました。" . PHP_EOL . PHP_EOL;
        $mailBody2 .= "お問い合わせ内容:" . PHP_EOL;
        $mailBody2 .= "\t商品番号 abc123について、サイズを教えてください。";

        $itemNumber = 'abc123';

        // ヒアドキュメント
        $mailBody3 = <<< MAIL
        お問い合わせを受け付けました。

        お問い合わせない内容:
                商品番号 {$itemNumber}について、'サイズ'を教えてください。
        MAIL;
    ?>
    <p><pre><?=$mailBody?></pre></p>
    <p><pre><?=$mailBody2?></pre></p>
    <p><pre><?=$mailBody3?></pre></p>
</body>
</html>