<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>変数</h1>
    <p>500円の商品</p>
    <?php $total = 500 * 1.10 ?>
    <p>税込価格: <?php echo $total; ?>円です。</p>
    <p>税込価格: <?php print $total; ?>円です。</p>
    <p>税込価格: <?=$total?>円です。</p>
    <p><?php echo '税込価格: ', $total, '円です。'?></p>
    <p><?php print '税込価格: '. $total . '円です。'?></p>
    <p><?='税込価格: ', $total, '円です。'?></p>
    <p><?php var_dump($total);?></p>
</body>
</html>