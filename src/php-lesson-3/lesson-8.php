<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>PHP実行時のオプション</h1>
    <?php
        ini_set('date.timezone', 'Asia/Tokyo');
        //日本時間の現在時刻を示します。
        echo date('Y-md H:i:s'), '<br>';

        ini_set('date.timezone', 'UTC');
        //協定世界時(UTC)の現在時刻を表示します。
        echo date('Y-m-d H:i:s'), '<br>';
    ?>
    <?php
        phpinfo();
    ?>
</body>
</html>