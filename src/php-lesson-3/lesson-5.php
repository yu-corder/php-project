<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>コールバック関数 クロージャー</h1>
    <?php
        $addFunction = function ($a, $b) {
            return $a + $b;
        };
        $total = $addFunction(10, 20);
    ?>
    <p>計算結果:<?=$total?></p>
    
    <?php
        $greeting = 'Good';
        //クロージャの定義。useを使って$greetingを引き継ぐ
        $greetingMaker = function ($time) use ($greeting) {
            print $greeting . '' . $time. '<br>';
        };

        //出力結果はGood Morning;
        $greetingMaker('Morning');

        //Beautiful Evenigじゃないよ!
        //useで引き継がれるのは関数が定義された時点の$greetingの値.
        //クロージャ定義時にuse(&$greeting)とリファレンス渡しすることでBeautiful Evenigになる。
        $greeting = 'Beautiful';
        $greetingMaker('Evening');
    ?>

    <?php
        /* あるユーザの購入履歴を表示する関数 */
        function printPurchased(array $items, callable $extraDataFunc): void 
        {
            echo '<table border = "1">';
            echo '<tr>';
            echo '<th>購入日</th>';
            echo '<th>商品名</th>';
            echo '<th>価格</th>';
            echo '<th>その他</th>';
            echo '</tr>';
            foreach ($items as $item) {
                echo '<tr>';
                echo '<td>', $item['date'], '</td>';
                echo '<td>', $item['name'], '</td>';
                echo '<td>', $item['price'], '</td>';
                //その他欄に印字するデータは呼び出し元が自由に設定できる。
                echo '<td>', $extraDataFunc($item), '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
        $items = [
            [
                'user-id' => 'tanaka-1234', //ユーザId
                'date' => '2021-05-27', //購入日
                'name' => 'ドレスシャツ', //商品名
                'price' => 2160, //価格
                'color' => 'white', //色
                'size' => 'M', //サイズ
                'item-number' => 91025, //商品番号
                'serial' => 'PLG01219' //製造番号
            ],
            [
                'user-id' => 'tanaka-1234', //ユーザId
                'date' => '2021-09-27', //購入日
                'name' => 'パジャマ', //商品名
                'price' => 1620, //価格
                'color' => 'red', //色
                'size' => 'S', //サイズ
                'item-number' => 90081, //商品番号
                'serial' => 'AMO80121' //製造番号
            ]
        ];

        echo '<h3>ユーザのマイページ内の購入履歴</h3>';
        printPurchased($items, function ($item) {
            return "色: {$item['color']} サイズ: {$item['size']}";
        });

        echo '<h3>販売業者専用ページ内の購入履歴</h3>';
        $extrFunc = function ($item) {
            return "ユーザID: {$item['user-id']} 商品NO: {$item['item-number']}";
        };
        printPurchased($items, $extrFunc);
    ?>
</body>
</html>