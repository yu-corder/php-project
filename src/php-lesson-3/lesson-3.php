<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ユーザー定義関数3 スコープ 型宣言</h1>
    <?php
        $num1 = 3;
        $num2 = 10;

        function add()
        {
            global $num1, $num2;
            return $num1 + $num2;
        }
        $result = add();
        echo $result;

        function number(int $a, int $b, ?string &$errorMessage): int
        {
            // return 'abc';

            if ($a <= 0 || $b <= 0) {
                $errorMessage = 'エラー: 正の整数を指定してください。';
            }

            $total = $a + $b;
            return $total;
        }

        $errorMessage = null;
        $result = number(3,10, $errorMessage);
        echo '結果:', $result, $errorMessage, PHP_EOL;

        $errorMessage = null;
        $result = number(3,-1, $errorMessage);
        echo '結果:', $result, $errorMessage, PHP_EOL;

        //$errorMessage = null;
        //$result = number(true, 10, $errorMessage);


        function calcPriceWithTax(int $price, float $tax = 0.08): float
        {
            $result = $price * (1 + $tax);
            return $result;
        }

        $priceWithTax = calcPriceWithTax(1000);
        echo "<p>結果: {$priceWithTax}</p>";

        $priceWithTax = calcPriceWithTax(1000, 0.05);
        echo "<p>結果: {$priceWithTax}</p>";


    ?>
   
</body>
</html>