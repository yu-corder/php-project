<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ユーザー定義関数1</h1>
    <?php
        function find($extensions) 
        {
            foreach ($extensions as $value) {
                if ($value == 'jpg' || $value == 'gif' || $value == 'png') {
                    return $value;
                }
            }
            return 'NOT FOUND';
        }

        $extensions = ['pdf', 'docx', 'gif', 'exe'];
        $found = find($extensions);
        echo $found;


        function checkNumber($value)
        {
            return is_numeric($value) && (int)$value > 0;
        }
        function add ($a, $b)
        {
            if (!checkNumber($a) || !checkNumber($b)) {
                return 'INVALID';
            }
            $total = $a + $b;
            return $total;
        }

        $result = add (3, 10);
        echo "<p>結果: {$result}</p>";

        $result = add(5, -4);
        echo "<p>結果: {$result}</p>";
    ?>
</body>
</html>