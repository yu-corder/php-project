<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ユーザー定義関数5</h1>
    <?php
        // ユーザー向けのその他の欄出力モード
        define('EXTRA_PRINT_MODE_USER', 1);

        // 販売事業者向けのその他の欄出力モード
        define('EXTRA_PRINT_MODE_VENDOR', 2);

        /* あるユーザの購入履歴を表示する関数*/
        function printPurchased(array $items, int $extraPrintMode): void
        {
            echo '<table border = "1">';
            echo '<tr>';
            echo '<th>購入日</th>';
            echo '<th>商品名</th>';
            echo '<th>価格</th>';
            echo '<th>その他</th>';
            echo '</tr>';
            foreach ($items as $item) {
                echo '<tr>';
                echo '<td>', $item['date'], '</td>';
                echo '<td>', $item['name'], '</td>';
                echo '<td>', $item['price'], '</td>';
                $extraData = '';
                if ($extraPrintMode === EXTRA_PRINT_MODE_USER) {
                    $extraData = "色: {$item['color']} サイズ: {$item['size']}";
                } elseif ($extraPrintMode === EXTRA_PRINT_MODE_VENDOR) {
                    $extraData = "商品NO: {$item['item-number']} 製造番号: {$item['serial']}";
                }
                echo '<td>', $extraData, '</td>';  
                echo '</tr>';          
            }
            echo '</table>';
        }

        $items = [
            [
                'user-id' => 'tanaka-1234', //ユーザId
                'date' => '2021-05-27', //購入日
                'name' => 'ドレスシャツ', //商品名
                'price' => 2160, //価格
                'color' => 'white', //色
                'size' => 'M', //サイズ
                'item-number' => 91025, //商品番号
                'serial' => 'PLG01219' //製造番号
            ],
            [
                'user-id' => 'tanaka-1234', //ユーザId
                'date' => '2021-09-27', //購入日
                'name' => 'パジャマ', //商品名
                'price' => 1620, //価格
                'color' => 'red', //色
                'size' => 'S', //サイズ
                'item-number' => 90081, //商品番号
                'serial' => 'AMO80121' //製造番号
            ]
        ];

        echo 'ユーザー向け購入履歴ページを出力します。<br>';
        printPurchased($items, EXTRA_PRINT_MODE_USER);

        echo '<br>販売事業者向け購入履歴ページを出力します。<br>';
        printPurchased($items, EXTRA_PRINT_MODE_VENDOR);
    ?>
</body>
</html>