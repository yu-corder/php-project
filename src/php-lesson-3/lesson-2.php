<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ユーザー定義関数2</h1>
    <?php
        function add(&$a)
        {
            $a += 10;
        }
        $argument = 5;
        add($argument);
        echo '$argumentは', $argument, 'です。';

        function checkNumber($value)
        {
            return is_numeric($value) && (int)$value > 0;
        }
        function number($a, $b, &$erroMessage)
        {
            if (!checkNumber($a)) {
                $a = 0;
                $erroMessage = 'エラー: １番目の引数が正の整数ではありません。';
            }
            if (!checkNumber($b)) {
                $b = 0;
                $erroMessage = 'エラー: ２番目の引数が正の整数ではありません。';
            }
            $total = $a + $b;
            return $total;
        }
        $erroMessage = null;
        $result = number(3, 10, $erroMessage);
        echo '結果:', $result, $erroMessage, PHP_EOL;

        $erroMessage = null;
        $result = number(4, -10, $erroMessage);
        echo '結果:', $result, $erroMessage, PHP_EOL;


    ?>
</body>
</html>