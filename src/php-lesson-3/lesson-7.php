<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>共通部品を外部ファイル化 共有</h1>
    <?php require_once dirname(__FILE__) . '/files/functions.php'; ?>
    <h3>todaのプロフィールページ</h3>
    <ul>
        <li>名前: toda</li>
        <li>年齢: <?=calcAge(19900811)?>才</li>
        <li>山口県</li>
    </ul>
    <h3>バナー広告がたくさんある記事ページ</h3>
    <p>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
    </p>
    <?php include(dirname(__FILE__) . '/files/banner-ad1.html'); ?>
    <p>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
    </p>
    <?php include(dirname(__FILE__) . '/files/banner-ad2.html'); ?>
    <p>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
        ここに記事のテキストが入ります。<br>
    </p>
    <?php include(dirname(__FILE__) . '/files/banner-ad3.html'); ?>
</body>
</html>