<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>ユーザー定義関数4 可変長引数</h1>
    <?php
        //複数の数値を足算して返す関数。
        //可変長引数は最後の引数にしか使えない
        function add(string $header, int ...$numbers): string
        {
            $total = 0;
            foreach ($numbers as $number) {
                $total += $number;
            }
            return $header . $total;
        }
        $result = add('計算結果:', 3, 2, 9, 1);
        echo $result;

        function add2(string $header, int $number1, int $number2, int $number3 = 0): string
        {
            $total = $number1 + $number2 + $number3;
            return $header . $total;
        }

        function add3(int ...$numbers): int
        {
            $total = 0;
            foreach ($numbers as $number) {
                $total += $number;
            }
            return $total;
        }
        $numbers = [3, 2, 4];
        $result = add2('計算結果:', ...$numbers);
        echo $result;
        $result = add3(...$numbers);
        echo $result;
    ?>
</body>
</html>